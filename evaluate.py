import matplotlib.pyplot as plt
import itertools
import pandas as pd
import numpy as np
import torch
import sys

from dataset import FERDataset
from torchvision import transforms, utils
from torch.utils.data import DataLoader
from sklearn.metrics import confusion_matrix, classification_report, balanced_accuracy_score, accuracy_score
from data_to_device import *
from models.model1 import FERModelAlexNet
from models.model2 import FERModel
from models.model3 import FERModelResNet


def plot_losses(history):
    """
    Plots a graph of training and validation loss against epoch number

    Args:
        history (dict): Training history of the model
    """
    train_losses = [x.get('train_loss') for x in history]
    val_losses = [x['val_loss'] for x in history]
    plt.plot(train_losses, '-bx')
    plt.plot(val_losses, '-rx')
    plt.xlabel('epoch')
    plt.ylabel('loss')
    plt.legend(['Training', 'Validation'])
    plt.title('Loss vs. No. of epochs');
    
def plot_lrs(history):
    """
    Plots a graph learning rate against batch number

    Args:
        history (dict): Training history of the model
    """
    lrs = np.concatenate([x.get('lrs', []) for x in history])
    plt.plot(lrs)
    plt.xlabel('Batch no.')
    plt.ylabel('Learning rate')
    plt.title('Learning Rate vs. Batch no.')

def create_loss_graph(model_name):
    """
    Plots a graph of training and validation loss against epoch number from log file

    Args:
        model_name (str): Path to saved model
    """
    contents = open("model.log", "r").read().split("\n")

    times = []
    accuracies = []
    losses = []

    val_accs = []
    val_losses = []

    for c in contents:
        if model_name in c:
            name, timestamp, loss, val_acc, val_loss, epoch = c.split(",")

            times.append(float(timestamp))
            losses.append(float(loss))

            val_accs.append(float(val_acc))
            val_losses.append(float(val_loss))


    plt.plot(losses, '-bx')
    plt.plot(val_losses, '-rx')
    plt.xlabel('epoch')
    plt.ylabel('loss')
    plt.legend(['Training', 'Validation'])
    plt.title('Loss vs. No. of epochs')


def plot_confusion_matrix(cm, classes, normalize=False, title='Confusion matrix', cmap=plt.cm.Blues):
    """
    Plots confusion matrix

    Args:
        cm (list): A list of list containing the computed confusion matrix
        classes (list): A list of classes
        normalize (bool): Set the confusion matrix to be normalized if True
        title (str): The title of the confusion matrix
        cmap (cmap): The colour map of the confusion matrix
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt), horizontalalignment="center", color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.grid(False)


@torch.no_grad()
def get_all_preds(model, loader):
    """
    Combines all predictions of model

    Args:
        model (model): The trained model
        loader (DeviceDataLoader): The dataset loaded into the selected device
    """
    all_preds = torch.tensor([])
    for batch in loader:
        images, labels = batch

        preds = model(images).to(torch.device('cpu'))
        all_preds = torch.cat(
            (all_preds, preds)
            ,dim=0
        )
    return all_preds


def load_model(model_arch, location):
    """
    Loads a saved model

    Args:
        model_arch (model): Class to load the model in
        location (str): Path to model
    """
    model = model_arch(1, 7)
    model_checkpoint = torch.load(location)
    model.to(device)
    model.load_state_dict(model_checkpoint['model'])
    history = model_checkpoint['history']
    return model, history, model_checkpoint['cm']


def plot_model_graphs(history, cm):
    """
    Plots all graphs

    Args:
        history (dict): Model history
        cm (list): Confusion matrix
    """
    plt.figure()
    plot_losses(history)
    plt.figure()
    plot_lrs(history)
    plt.figure(figsize=(7,7))
    plot_confusion_matrix(cm, classes.values())
    plt.show()


def get_metrics(y_label, preds):
    """
    Computes and prints metrics of model

    Args:
        y_label (list): List of labels
        preds (list): List of model predictions
    """
    bascore = balanced_accuracy_score(y_label, preds)
    print(f"Balanced accuracy: {bascore}")
    accuracy = accuracy_score(y_label, preds)
    print(f"Accuracy: {accuracy}")
    creport = classification_report(y_label, preds, target_names=['Angry', 'Disgust', 'Fear', 'Happy', 'Sad', 'Surprise', 'Neutral'])
    print(creport)


def get_saliency_map(model):
    """
    Plots a saliency map of a specific image

    Args:
        model (model): Trained model
    """
    model.eval()

    # select an image
    image, lbl = train_data[7]
    image = image.reshape(1, 1, 48, 48)
    image = image.to(device)

    # set the image to keep track of gradient
    image.requires_grad_()
    output = model(image)
    score_max_index = output.argmax()
    score_max = output[0,score_max_index]
    score_max.backward()

    # get the saliency of the image
    saliency, _ = torch.max(image.grad.data.abs(), dim=1)
    saliency = saliency.to("cpu")

    # plot the image and its saliency map
    fig, ax = plt.subplots(1, 2)
    ax[0].imshow(image.cpu().detach().squeeze(), cmap='gray')
    ax[0].axis('off')
    ax[1].imshow(saliency[0], cmap='gray')
    ax[1].axis('off')
    plt.show()


if __name__ == "__main__":
    model_path = {
        1: "saved_models\model-1620066366-alexnet.pth",
        2: "saved_models\model-1620066628-nonresnet.pth",
        3: "saved_models\model-1620066103-resnet.pth"
    }
    if len(sys.argv) != 2:
        print("Incorrect arguments specified. Usage: evaluate.py <model-number>")
        sys.exit(1)
    if sys.argv[1] == "model1":
        path = model_path[1]
        model_arc = FERModelAlexNet
    elif sys.argv[1] == "model2":
        path = model_path[2]
        model_arc = FERModel
    elif sys.argv[1] == "model3":
        path = model_path[3]
        model_arc = FERModelResNet
    else:
        print("There is no such model. Usage: evaluate.py <model-number>")
        sys.exit(1)

    # read from csv file
    df = pd.read_csv("fer2013.csv")

    # initialise classes
    classes = {
        0: 'Angry', 1: 'Disgust', 2: 'Fear', 3: 'Happy', 4: 'Sad', 5: 'Surprise', 6: 'Neutral'
    }

    # initialise train and test data
    df_train = pd.concat([df[(df.Usage == 'Training')], df[df.Usage == 'PublicTest']], ignore_index=True).drop(['Usage'], axis=1)
    df_test = df[df.Usage == 'PrivateTest'].drop(['Usage'], axis=1).reset_index().drop(['index'], 1)

    # differentiating between labels and images
    train_images = df_train.iloc[:, 1]
    train_labels = df_train.iloc[:, 0]
    test_images = df_test.iloc[:, 1]
    test_labels = df_test.iloc[:, 0]


    # initialise transforms for train and test set
    train_trfm = transforms.Compose(
        [
            transforms.ToPILImage(),
            transforms.Grayscale(num_output_channels=1),
            transforms.RandomCrop(48, padding=4, padding_mode='reflect'),
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
            transforms.Normalize((0.5), (0.5), inplace=True)
        ])
    val_trfm = transforms.Compose(
        [
            transforms.ToPILImage(),
            transforms.Grayscale(num_output_channels=1),
            transforms.ToTensor(),
            transforms.Normalize((0.5), (0.5))
        ])

    # wrap dataset into the class
    train_data = FERDataset(train_images, train_labels, train_trfm)
    val_data = FERDataset(test_images, test_labels, val_trfm)

    batch_num = 64

    train_dl = DataLoader(train_data, batch_num, shuffle=True)
    val_dl = DataLoader(val_data, batch_num*2)

    # initialise device
    device = get_default_device()

    # load model
    model, history, cm = load_model(model_arc, path)

    # set model to evaluation mode
    model.eval()

    # move train and validation set to device
    train_dl = DeviceDataLoader(train_dl, device)
    val_dl = DeviceDataLoader(val_dl, device)

    # initialise true labels of the test set
    y_label = torch.Tensor([int(x) for x in val_data.y])

    # get predictions for the test set
    with torch.no_grad():
        prediction_loader = DataLoader(val_data, batch_size=400)
        prediction_loader = DeviceDataLoader(prediction_loader, device)
        train_preds = get_all_preds(model, prediction_loader)
    
    # compute confusion matrix
    cm = confusion_matrix(y_label, train_preds.argmax(dim=1))

    # plot graphs
    plot_model_graphs(history, cm)
    get_metrics(y_label, train_preds.argmax(dim=1))
    get_saliency_map(model)