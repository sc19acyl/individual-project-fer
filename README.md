# Individual project 2020/2021
## Author: Adrian Chen Young Lee

## Build and run the code

Anaconda and PyTorch is assumed to be installed on the machine.

To see the evaluation of the models:
    `$ evaluate.py <model-name>`

eg: evaluate.py model1
The only models are 'model1', 'model2', and 'model3'


To train a new network:
    `$ train.py <model-name>`

This selects the model architecture that the training will be done on
The only models are 'model1', 'model2', and 'model3'


Alternatively, the Jupyter Notebook can be accessed. It contains the same code as the python files. 
