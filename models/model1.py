from models.base import *
import numpy as np
import time

def get_output_shape(model, image_dim):
    """
    Computes the output shape

    Args:
        model (model): The model architecture
        image_dim (tuple): The shape of the image in tuple
    """
    return model(torch.rand(*(image_dim))).data.shape

class FERModelAlexNet(FERBase):
    """
    Extends FERBase class
    """
    def __init__(self, in_channels, num_classes):
        super().__init__()
        self.MODEL_NAME = f"model-{int(time.time())}"
        
        self.cnn = nn.Sequential(
            conv_block(in_channels, 64, pool=True),
            conv_block(64, 128, pool=True),
            conv_block(128, 256, pool=True),
            conv_block(256, 512, pool=True)
        )

        conv_out = get_output_shape(self.cnn, (1, 1, 48, 48))
        
        self.classifier = nn.Sequential(
            nn.Flatten(),
            nn.Linear(np.prod(list(conv_out)), 4096),
            nn.ReLU(inplace=True),
            nn.Linear(4096, 4096),
            nn.ReLU(inplace=True),
            nn.Linear(4096, num_classes),
        )
        
        
    def forward(self, xb):
        x = self.cnn(xb)
        
        return self.classifier(x)