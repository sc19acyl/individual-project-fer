import torch
import torch.nn as nn
import torch.nn.functional as F

def accuracy(outputs, labels):
    """
    Computes accuracy

    Args:
        outputs (tensor): A tensor of predictions from the model
        labels (tensor): A tensor of true labels
    """
    _, preds = torch.max(outputs, dim=1)
    return torch.tensor(torch.sum(preds==labels).item()/len(preds))

class FERBase(nn.Module):
    
    def training_step(self, batch):
        """
        Makes prediction and returns loss

        Args:
            batch (tuple): A batch of (images, labels)
        """
        images, labels = batch
        out = self(images)
        loss = F.cross_entropy(out, labels)
        return loss
    
    def validation_step(self, batch):
        """
        Computes loss and accuracy, adds new entries to val_loss and val_acc

        Args:
            batch (tuple): A batch of (images, labels)
        """
        images, labels = batch
        out = self(images)
        loss = F.cross_entropy(out, labels)
        acc = accuracy(out, labels)
        return {'val_loss': loss.detach(), 'val_acc': acc}
    
    def validation_epoch_end(self, outputs):
        """
        Returns current epoch accs and losses

        Args:
            outputs (list): List of val_loss and val_acc
        """
        batch_losses = [x['val_loss'] for x in outputs]
        epoch_loss = torch.stack(batch_losses).mean()
        batch_accs = [x['val_acc'] for x in outputs]
        epoch_acc = torch.stack(batch_accs).mean()
        return {'val_loss': epoch_loss.item(), 'val_acc': epoch_acc.item()}
    
    def epoch_end(self, epoch, result):
        """
        Prints training details at the end of epoch

        Args:
            epoch (int): Current epoch number
            result (dict): Dictionary containing learning rate, accuracy and loss
        """
        print("Epoch [{}], last_lr: {:.5f}, train_loss: {:.4f}, val_loss: {:.4f}, val_acc: {:.4f}".format(
            epoch, result['lrs'][-1], result['train_loss'], result['val_loss'], result['val_acc']))


def conv_block(in_chnl, out_chnl, pool=False, padding=1):
    """
    Creates a convolutional block

    Args:
        in_chnl (int): Number of input channels
        out_chnl (int): Number of output channels
        pool (bool): Max pool
        padding (int): Padding dimension
    """
    layers = [
        nn.Conv2d(in_chnl, out_chnl, kernel_size=3, padding=padding),
        nn.BatchNorm2d(out_chnl),
        nn.ReLU(inplace=True)]
    if pool: layers.append(nn.MaxPool2d(2))
    return nn.Sequential(*layers)