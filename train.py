import pandas as pd 
import matplotlib.pyplot as plt
import numpy as np
import torch, os
import torch.nn as nn
import time
from dataset import FERDataset
from torchvision import transforms, utils
from torch.utils.data import DataLoader

from data_to_device import *

from models.model1 import FERModelAlexNet
from models.model2 import FERModel
from models.model3 import FERModelResNet


@torch.no_grad()
def evaluate(model, val_loader):
    """
    Evaluate model given the current parameters
    """
    model.eval()
    outputs = [model.validation_step(batch) for batch in val_loader]
    return model.validation_epoch_end(outputs)


def get_lr(optimizer):
    """
    Get learning rate
    """
    for param_group in optimizer.param_groups:
        return param_group['lr']


def fit(epochs, max_lr, model, train_loader, val_loader, weight_decay=0, grad_clip=None, opt_func=torch.optim.Adam):
    """
    Trains the model and writes a log file at the end of every epoch

    Args:
        epochs (int): The number of epochs to train the model
        max_lr (float): The maximum learning rate for training
        model (nn.Module): The model class
        train_loader (DeviceDataLoader): The training set
        val_loader (DeviceDataLoader): The validation set
        weight_decay (float): Weight decay
        grad_clip (float): Gradient clipping
        opt_func (torch.optim): The type of optimizer
    """
    # keeping track of the results
    history = []
    
    # intialize optimizer with weight decay
    optimizer = opt_func(model.parameters(), max_lr, weight_decay=weight_decay)

    # initialize 1cycle scheduler
    sched = torch.optim.lr_scheduler.OneCycleLR(optimizer, max_lr, epochs=epochs, steps_per_epoch=len(train_loader))
    
    with open("model.log", 'a') as f:
        for epoch in range(epochs):
            # set model to training mode
            model.train()
            train_losses = []
            lrs = []
            for batch in train_loader:
                loss = model.training_step(batch)
                train_losses.append(loss)
                loss.backward()
                
                # gradient clipping
                if grad_clip:
                    nn.utils.clip_grad_value_(model.parameters(), grad_clip)
                    
                optimizer.step()
                optimizer.zero_grad()
                
                # record the lr
                lrs.append(get_lr(optimizer))
                sched.step()
                
            # validation
            result = evaluate(model, val_loader)

            # record result
            result['train_loss'] = torch.stack(train_losses).mean().item()
            result['lrs'] = lrs

            # end of epoch recording
            model.epoch_end(epoch, result)

            # writing to log file
            f.write(f"{model.MODEL_NAME},{round(time.time(),3)},{result['train_loss']},{result['val_acc']},{result['val_loss']},{epoch}\n")
            history.append(result)

    return history

def save_model(model):
    """
    Saves trained model with history of model and confusion matrix
    
    Args:
        model (model): Trained model
    """
    print("Saving model....")
    torch.save({
        'model': model.state_dict(),
        'history': history,
        'cm': cm
    }, f"{model.MODEL_NAME}.pth")
    print(f"Model saved with name: {model.MODEL_NAME}.pth")

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Incorrect arguments specified. Usage: train.py <model-number>")
        sys.exit(1)
    if sys.argv[1] == "model1":
        model_arc = FERModelAlexNet
    elif sys.argv[1] == "model2":
        model_arc = FERModel
    elif sys.argv[1] == "model3":
        model_arc = FERModelResNet
    else:
        print("There is no such model. Usage: train.py <model-number>")
        sys.exit(1)
    # read from csv file
    df = pd.read_csv("fer2013.csv")

    # initialise classes
    classes = {
        0: 'Angry', 1: 'Disgust', 2: 'Fear', 3: 'Happy', 4: 'Sad', 5: 'Surprise', 6: 'Neutral'
    }

    # initialise train and test data
    df_train = pd.concat([df[(df.Usage == 'Training')], df[df.Usage == 'PublicTest']], ignore_index=True).drop(['Usage'], axis=1)
    df_test = df[df.Usage == 'PrivateTest'].drop(['Usage'], axis=1).reset_index().drop(['index'], 1)

    # differentiating between labels and images
    train_images = df_train.iloc[:, 1]
    train_labels = df_train.iloc[:, 0]
    test_images = df_test.iloc[:, 1]
    test_labels = df_test.iloc[:, 0]


    # initialise transforms for train and test set
    train_trfm = transforms.Compose(
        [
            transforms.ToPILImage(),
            transforms.Grayscale(num_output_channels=1),
            transforms.RandomCrop(48, padding=4, padding_mode='reflect'),
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
            transforms.Normalize((0.5), (0.5), inplace=True)
        ])
    val_trfm = transforms.Compose(
        [
            transforms.ToPILImage(),
            transforms.Grayscale(num_output_channels=1),
            transforms.ToTensor(),
            transforms.Normalize((0.5), (0.5))
        ])

    # wrap dataset into the class
    train_data = FERDataset(train_images, train_labels, train_trfm)
    val_data = FERDataset(test_images, test_labels, val_trfm)

    batch_num = 64

    train_dl = DataLoader(train_data, batch_num, shuffle=True)
    val_dl = DataLoader(val_data, batch_num*2)

    # initialise device
    device = get_default_device()

    # initialise model and move model to device
    model = model_arc(1, 7)
    train_dl = DeviceDataLoader(train_dl, device)
    val_dl = DeviceDataLoader(val_dl, device)
    to_device(model, device)


    # set max learning rate, gradient clipping and weight decay values
    max_lr = 0.001
    grad_clip = 0.1
    weight_decay = 1e-4

    # start training
    history = fit(30, max_lr, model, train_loader=train_dl, val_loader=val_dl, weight_decay=weight_decay, grad_clip=grad_clip)

    save_model(model)