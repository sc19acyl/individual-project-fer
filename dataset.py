import numpy as np
from torch.utils.data import Dataset

class FERDataset(Dataset):
    """
    Wraps dataset into a class for easy access of data
    """
    def __init__(self, images, labels, transforms):
        self.X = images
        self.y = labels
        self.transforms = transforms
        
    def __len__(self):
        """
        Length of dataset
        """
        return len(self.X)
    
    def __getitem__(self, i):
        """
        Returns data in a tuple (data, label)
        """
        data = [int(m) for m in self.X[i].split(' ')]
        data = np.asarray(data).astype(np.uint8).reshape(48,48,1)
        data = self.transforms(data)
        label = self.y[i]
        return (data, label)